# Makefile for EEE
# Note: The order of the lines below is important

EXCLUDE_VERSIONS  += 3.14.12.5

include ${EPICS_ENV_PATH}/module.Makefile

EXCLUDE_ARCHS     += eldk

USR_DEPENDENCIES  = asyn,4.31.0
USR_DEPENDENCIES += calc,3.6.1
USR_DEPENDENCIES += seq,2.1.10
USR_DEPENDENCIES += streamdevice,2.7.7
