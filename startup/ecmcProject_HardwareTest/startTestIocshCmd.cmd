############################################################
############# Intro
#
# Simple test IOC for remote IOC access using Asyn record and 
# the ECMC comamnd "Cfg.IocshCmd=<ioccmd>"
#
# See last rows in this file for how to execute an example cmd
#
# Note: Sadly records cannot be loaded dynamically in EPICS
# 
############################################################
############# Init

< ../general/require_E3
< ../general/init

##############################################################################
############# ASYN Configuration:

epicsEnvSet("ECMC_MOTOR_PORT",    "$(SM_MOTOR_PORT=MCU1)")
epicsEnvSet("ECMC_ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("ECMC_PREFIX",        "$(SM_PREFIX=IOC2:)")

ecmcAsynPortDriverConfigure($(ECMC_ASYN_PORT),1000,0,0,100)
asynOctetSetOutputEos("${ECMC_ASYN_PORT}", -1, ";\n")
asynOctetSetInputEos("${ECMC_ASYN_PORT}", -1, ";\n")

asynSetTraceMask("${ECMC_ASYN_PORT}", -1, 0x41)
asynSetTraceIOMask("${ECMC_ASYN_PORT}", -1, 2)
asynSetTraceIOMask("${ECMC_ASYN_PORT}", -1, 6)

asynSetTraceInfoMask("${ECMC_ASYN_PORT}", -1, 1)

EthercatMCCreateController("${ECMC_MOTOR_PORT}", "${ECMC_ASYN_PORT}", "32", "200", "1000", "")

##############################################################################
############# Misc settings:

# Disable function call trace printouts
ecmcConfigOrDie "Cfg.SetEnableFuncCallDiag(0)"

# Disable on change printouts from objects (enable for easy logging)
ecmcConfigOrDie "Cfg.SetTraceMaskBit(15,0)"

# Choose to generate EPICS-records for EtherCAT-entries 
# (For records use ECMC_GEN_EC_RECORDS="-records" otherwise ECMC_GEN_EC_RECORDS="") 
epicsEnvSet("ECMC_GEN_EC_RECORDS",          "")

# Choose to generate EPICS-records for ax-entries (PosAct, SetPos,..)
# (For records use ECMC_GEN_AX_RECORDS="-records" otherwise ECMC_GEN_AX_RECORDS="") 
epicsEnvSet("ECMC_GEN_AX_RECORDS",          "-records")

# Update records in 10ms (100Hz) 
epicsEnvSet("ECMC_SAMPLE_RATE_MS",       "10")
epicsEnvSet("ECMC_TSE",       "-2")

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

##############################################################################
############# Load general controller level records:

< ../general/general

##############################################################################
############# Go to runtime:

ecmcConfigOrDie "Cfg.SetAppMode(1)"

##############################################################################
############# Example: 
#
# Commands to test iocshCmd over asyn record with ecmc ASCII command "Cfg.IocshCmd=<cmd>":
# 1: Set use of Binary output of Asyn record (to allow larger size than 40):
# caput -S IOC2:MCU-Cmd.OFMT "Binary"
# 2: Test simple dbl command (EPICS variables can be used also, ECMC_SLAVE_NUM):
# caput -S IOC2:MCU-Cmd.BOUT 'Cfg.IocshCmd=dbl'
# 3: Check response. Ends up in AINP since asyn input is set to ASCII (IFMT=ASCII) as default
# caget -S  IOC2:MCU-Cmd.AINP
