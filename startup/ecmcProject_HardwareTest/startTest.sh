#!/bin/sh

if [ "$#" -lt 1 ] || [ "$#" -gt 3 ]
then
      echo ""
      echo " $0 <HW Type> <slave Id> <master Id>"      
      echo "  <HW Type>  : String describing type of hardware"
      echo "  <slave id> : Slave index of slave to test (optional)"
      echo "  <master id>: Master index of slave to test (optional, default = 0)"
      echo ""
      echo " Example 1 args: EL2004. Script will try to find slave id of first EL2004 on master 0"
      echo " $0 EL2004"
      echo " Example 3 args: EL2004 with slave id 13 on master 0"
      echo " $0 EL2004 13"
      echo " Example 3 args: EL2004 with slave id 5 on master 1"
      echo " $0 EL2004 5 1"
      echo ""
      exit 1
fi

hw_name=$1

if [ "$#" -eq 1 ]
then  
  get_slave_command=$(ethercat slaves | grep $1 | grep PREOP | awk  '{print $1}')
  slave_id=$get_slave_command  
  master_id=0  
fi

if [ "$#" -eq 2 ]
then      
  slave_id=$2
  master_id="0"
fi

if [ "$#" -eq 3 ]
then      
  slave_id=$2
  master_id=$3
fi

export ECMC_TEST_HW_NAME=$hw_name
export ECMC_TEST_HW_SLAVE_ID=$slave_id
export ECMC_TEST_HW_MASTER_ID=$master_id

echo "Starting test:"
echo "  -hardware  = $ECMC_TEST_HW_NAME"
echo "  -slave id  = $ECMC_TEST_HW_SLAVE_ID"
echo "  -master id = $ECMC_TEST_HW_MASTER_ID"

echo "Do you wish to run test?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) iocsh.bash startTest.cmd;;
        No ) exit;;
    esac
done