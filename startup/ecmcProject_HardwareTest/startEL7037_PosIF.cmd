
< ../general/require_E3
< ../general/init

############################################################
############# ASYN Configuration:

epicsEnvSet("ECMC_MOTOR_PORT",    "$(SM_ECMC_MOTOR_PORT=MCU1)")
epicsEnvSet("ECMC_ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("ECMC_PREFIX",        "$(SM_ECMC_PREFIX=IOC2:)")

#drvAsynECMCPortConfigure("$(ECMC_ASYN_PORT)", 0, 0, 0)
ecmcAsynPortDriverConfigure($(ECMC_ASYN_PORT),1000,0,0,100)
asynOctetSetOutputEos("$(ECMC_ASYN_PORT)", -1, ";\n")
asynOctetSetInputEos("$(ECMC_ASYN_PORT)", -1, ";\n")

asynSetTraceMask("$(ECMC_ASYN_PORT)", -1, 0x41)
asynSetTraceIOMask("$(ECMC_ASYN_PORT)", -1, 2)
asynSetTraceIOMask("$(ECMC_ASYN_PORT)", -1, 6)
asynSetTraceInfoMask("$(ECMC_ASYN_PORT)", -1, 1)

EthercatMCCreateController("$(ECMC_MOTOR_PORT)", "$(ECMC_ASYN_PORT)", "32", "200", "1000", "")

############################################################
############# Misc settings:

#Disable function call trace printouts
ecmcConfigOrDie "Cfg.SetEnableFuncCallDiag(0)"

# enable on change printouts from objects (for easy logging)
#ecmcConfigOrDie "Cfg.SetTraceMaskBit(15,1)"

# enable jitter printouts
#ecmcConfigOrDie "Cfg.SetTraceMaskBit(13,1)"

# Choose to generate EPICS-records for EtherCAT-entries 
# (For records use ECMC_GEN_EC_RECORDS="-records" otherwise ECMC_GEN_EC_RECORDS="") 
epicsEnvSet("ECMC_GEN_EC_RECORDS",          "-records")

# Update records in 10ms (100Hz) 
epicsEnvSet("ECMC_SAMPLE_RATE_MS",       "10")

############################################################
############# Configure hardware:

epicsEnvSet("ECMC_EC_MASTER_ID"          "0")

#Choose master
ecmcConfigOrDie "Cfg.EcSetMaster($(ECMC_EC_MASTER_ID))"

#Configure EL7037 stepper drive terminal, motor 2
epicsEnvSet("ECMC_EC_SLAVE_NUM",              "7")
< ../hardware/ecmcEL7037_PosIF$(ECMC_GEN_EC_RECORDS)

#Configure motor for EL7037
< ../hardware/ecmcEL7037-Motor-Trinamic-QMot-QSH4218-41-10-035

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
############# Write outputs in order to power switches (see elec. drawings):

# Set to one to be able to check if the EtherCAT is OK
ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

##############################################################################
############# Load general controller level records:

< ../general/general

##############################################################################
############# Go to runtime:

ecmcConfig "Cfg.SetAppMode(1)"

# Below is a sequence describing what records to use for absolute positioning! 
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-StartType 1
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-AccCmd 1000
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-DecCmd 1000
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-VelCmd 500
# Enable
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Drv-Cmd 1
# Move one rev (200step*64microstepping)
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-PosCmd 12800
#Trigg motion by toggle
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-Cmd 0
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-Cmd 1
# Get actual position alt 1
#mcu002> dbgf IOC2:ec0-s7-EL7037_PosIF-Enc-PosAct
# Get actual position alt 2
#mcu002> dbgf IOC2:ec0-s7-EL7037_PosIF-Pos-Pos
# Stop motion
#mcu002> dbpf IOC2:ec0-s7-EL7037_PosIF-Pos-Cmd 0

