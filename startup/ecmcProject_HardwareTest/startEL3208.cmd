############################################################
############# Init

< ../general/require_E3
< ../general/init

###############################################################################
############# ASYN Configuration:

epicsEnvSet("ECMC_MOTOR_PORT",    "$(SM_MOTOR_PORT=MCU1)")
epicsEnvSet("ECMC_ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("ECMC_PREFIX",        "$(SM_ECMC_PREFIX=IOC2:)")

ecmcAsynPortDriverConfigure($(ECMC_ASYN_PORT),1000,0,0,100)

asynOctetSetOutputEos("$(ECMC_ASYN_PORT)", -1, ";\n")
asynOctetSetInputEos("$(ECMC_ASYN_PORT)", -1, ";\n")

asynSetTraceMask("$(ECMC_ASYN_PORT)", -1, 0x41)
asynSetTraceIOMask("$(ECMC_ASYN_PORT)", -1, 6)
asynSetTraceInfoMask("$(ECMC_ASYN_PORT)", -1, 1)

###############################################################################
############# Misc settings:
# Disable function call trace printouts
ecmcConfigOrDie "Cfg.SetEnableFuncCallDiag(0)"

# Disable on change printouts from objects (for easy logging)
ecmcConfigOrDie "Cfg.SetTraceMaskBit(15,0)"

# Disable on command transform diag
ecmcConfigOrDie "Cfg.SetTraceMaskBit(7,0)"

# Choose to generate EPICS-records for EtherCAT-entries 
# (For records use ECMC_GEN_EC_RECORDS="-epicsrecords" otherwise ECMC_GEN_EC_RECORDS="") 
epicsEnvSet("ECMC_GEN_EC_RECORDS",          "-records")

# Choose to generate EPICS-records for ax-entries (PosAct, SetPos,..)
# (For records use ECMC_GEN_AX_RECORDS="-records" otherwise ECMC_GEN_AX_RECORDS="") 
epicsEnvSet("ECMC_GEN_AX_RECORDS",          "-records")

# Update records in 10ms (100Hz) 
epicsEnvSet("ECMC_SAMPLE_RATE_MS",       "10")

###############################################################################
############# Configure hardware:

epicsEnvSet("ECMC_EC_MASTER_ID"               "0")

#Choose master
ecmcConfigOrDie "Cfg.EcSetMaster($(ECMC_EC_MASTER_ID))"

# Configure EL3214 analog input hardware
epicsEnvSet("ECMC_EC_SLAVE_NUM",              "27")
< ../hardware/ecmcEL3208$(ECMC_GEN_EC_RECORDS)

############# Configure sensors:
# Common for all channels
< ../hardware/ecmcEL32XX-Sensor-PT100-common

# Configure channel 1 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8000")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Configure channel 2 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8010")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Configure channel 3 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8020")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Configure channel 4 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8030")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Configure channel 5 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8040")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Configure channel 6 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8050")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Configure channel 7 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8060")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Configure channel 8 with S+S Regeltechnik HFT50 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8070")
< ../hardware/ecmcEL3208-Sensor-chX_S+S_RegelTechnik_HTF50_PT1000

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

###############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(10)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"

##############################################################################
############# Load general controller level records:

< ../general/general

##############################################################################
############# Go to runtime:

ecmcConfig "Cfg.SetAppMode(1)"
