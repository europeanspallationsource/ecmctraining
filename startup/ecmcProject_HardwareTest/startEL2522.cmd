############################################################
############# Init

< ../general/require_E3
< ../general/init

############################################################
############# ASYN Configuration:

epicsEnvSet("ECMC_MOTOR_PORT",    "$(SM_MOTOR_PORT=MCU1)")
epicsEnvSet("ECMC_ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("ECMC_PREFIX",        "$(SM_ECMC_PREFIX=IOC_ANDERS:)")

ecmcAsynPortDriverConfigure($(ECMC_ASYN_PORT),1000,0,0,100)

asynOctetSetOutputEos("$(ECMC_ASYN_PORT)", -1, ";\n")
asynOctetSetInputEos("$(ECMC_ASYN_PORT)", -1, ";\n")

asynSetTraceMask("$(ECMC_ASYN_PORT)", -1, 0x41)
asynSetTraceIOMask("$(ECMC_ASYN_PORT)", -1, 6)
asynSetTraceInfoMask("$(ECMC_ASYN_PORT)", -1, 1)

EthercatMCCreateController("$(ECMC_MOTOR_PORT)", "$(ECMC_ASYN_PORT)", "32", "200", "1000", "")

############################################################
############# Misc settings:
# Choose to generate EPICS-records for EtherCAT-entries 
# (For records use ECMC_GEN_EC_RECORDS="-records" otherwise ECMC_GEN_EC_RECORDS="") 
epicsEnvSet("ECMC_GEN_EC_RECORDS",          "-records")

epicsEnvSet("ECMC_GEN_AX_RECORDS",          "-records")

# Update records in 10ms (100Hz) 
epicsEnvSet("ECMC_SAMPLE_RATE_MS",       "10")

############################################################
############# Configure hardware:

epicsEnvSet("ECMC_EC_MASTER_ID"          "0")

#Choose master
ecmcConfigOrDie "Cfg.EcSetMaster($(ECMC_EC_MASTER_ID))"

# Add output for enable of step direction drive (also to test reduce torque and brake)
epicsEnvSet("ECMC_EC_SLAVE_NUM",              "18")
epicsEnvSet("ECMC_EC_SLAVE_NUM_OUTPUT",       "$(ECMC_EC_SLAVE_NUM)")

< ../hardware/ecmcEL2004${ECMC_GEN_EC_RECORDS}

epicsEnvSet("ECMC_EC_SLAVE_NUM",              "19")
epicsEnvSet("ECMC_EC_SLAVE_NUM_DRIVE",        "$(ECMC_EC_SLAVE_NUM)")
< ../hardware/ecmcEL2522${ECMC_GEN_EC_RECORDS}

# Configure for step direction
< ../hardware/ecmcEL2522-stepDirectionDrive

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
############# Configuration of motor 1

# Only run one axis at the time since only 4 outputs (need 6 to run 2 axis)

# Custom settings for Axis 1
#< startEL2522_Axis1

# Apply configurations to ECMC
#< ../motion/ecmc_axis$(ECMC_GEN_AX_RECORDS)

# Custom settings for Axis 2
< startEL2522_Axis2

# Apply configurations to ECMC
< ../motion/ecmc_axis$(ECMC_GEN_AX_RECORDS)

###############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(10)"
ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"

##############################################################################
############# Load general controller level records:

< ../general/general

##############################################################################
############# Go to runtime:

ecmcConfigOrDie "Cfg.SetAppMode(1)"
